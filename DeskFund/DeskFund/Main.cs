﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using DeskFund.model;
using System.Threading;

namespace DeskFund
{
    public partial class Main : DevExpress.XtraEditors.XtraForm
    {
        public Main()
        {
            InitializeComponent();
            getFundData();
            this.timer1.Enabled = true;
            this.timer2.Interval = ConvertUtil.ToInt32(IniHelper.ReadValue("Interval"),10000);
            this.timer2.Enabled = true;
        }

        #region 窗体隐藏部分
        void Timer1Tick(object sender, EventArgs e)
        {
            if (this.Bounds.Contains(Cursor.Position))
            {
                switch (this.StopAnhor)
                {
                    case AnchorStyles.Top:
                        this.Location = new Point(this.Location.X, 0);
                        break;
                    case AnchorStyles.Left:
                        this.Location = new Point(0, this.Location.Y);
                        break;
                    case AnchorStyles.Right:
                        this.Location = new Point(Screen.PrimaryScreen.Bounds.Width - this.Width, this.Location.Y);
                        break;
                }
            }
            else
            {
                switch (this.StopAnhor)
                {
                    case AnchorStyles.Top:
                        this.Location = new Point(this.Location.X, (this.Height - 4) * (-1));
                        break;
                    case AnchorStyles.Left:
                        this.Location = new Point((this.Width - 4) * (-1), this.Location.Y);
                        break;
                    case AnchorStyles.Right:
                        this.Location = new Point(Screen.PrimaryScreen.Bounds.Width - 4, this.Location.Y);
                        break;
                }
            }
        }

        internal AnchorStyles StopAnhor = AnchorStyles.None;
        private void mStopAnhor()
        {
            if (this.Top <= 0)
            {
                StopAnhor = AnchorStyles.Top;
            }
            else if (this.Left <= 0)
            {
                StopAnhor = AnchorStyles.Left;
            }
            else if (this.Right >= Screen.PrimaryScreen.Bounds.Width)
            {
                StopAnhor = AnchorStyles.Right;
            }
            else
            {
                StopAnhor = AnchorStyles.None;
            }
        }

        void MainFormLocationChanged(object sender, EventArgs e)
        {
            this.mStopAnhor();
        }
        #endregion

        /// <summary>
        /// 获取基金数据并显示
        /// </summary>
        public void getFundData() 
        {
            string url = IniHelper.ReadValue("Config","URL");
            string fundListStr = IniHelper.ReadValue("Config","FundCode");
            ArrayList fundList = new ArrayList(fundListStr.Split(','));
            ArrayList fundDataList = new ArrayList();
            if (fundList.Count > 0)
            {
                foreach (string id in fundList)
                {
                    //获取json数据
                    RequestMessage requestMessage = RequestHelper.RequestGet(url.Replace("*", id));
                    if (requestMessage.Status == System.Net.HttpStatusCode.OK)
                    {
                        string json = requestMessage.Message;
                        json = json.Replace("jsonpgz(", "");
                        json = json.Replace(");", "");
                        //解析json数据
                        FundData fundData = JsonHelper.JsonDeserializeBySingleData<FundData>(json);
                        //放入table
                        if (fundData != null)
                        {
                            fundDataList.Add(fundData);
                        }
                    }
                }
                if (fundDataList.Count > 0)
                {
                    gridControl1.DataSource = fundDataList;
                    gridView1.OptionsBehavior.AutoExpandAllGroups = true;
                }
            }
        }
        /// <summary>
        /// 对显示数据涨跌进行单独背景颜色设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridView1_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName == "Gszzl")
            {
                string GszzlStr = gridView1.GetRowCellDisplayText(e.RowHandle, gridView1.Columns["Gszzl"]);
                if (ConvertUtil.ToDecimal(GszzlStr,0) < 0)
                {
                    e.Appearance.BackColor = Color.Green;
                    e.Appearance.BackColor2 = Color.LightCyan;
                }
                else
                {
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.BackColor2 = Color.LightCyan;
                }
            }
        }
        /// <summary>
        /// 启动定时器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_Tick(object sender, EventArgs e)
        {
            getFundData();
        }
    }
}