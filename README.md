![DeskFund](icon.png)
##DeskFund是什么?
一个桌面显示基金涨跌情况的小软件


##DeskFund是什么有哪些功能？

* 显示当前基金的净值和估算净值
    *  目前只支持天天基金网数据显示,后续增加其他网站数据
    *  目前显示基金数据需要在配置文件中自己数据基金代码,逗号隔开
    *  配置文件中可以设置更新时间间隔

##有问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

* 邮件(lipeng3g#gmail.com, 把#换成@)
* QQ: 654149394


##感激
感谢以下的项目,排名不分先后

* [_YMW](http://www.cnblogs.com/_ymw/archive/2010/10/19/1855272.html) 
* [zhaoming](#) 

##关于作者

```javascript
  var user = {
    nickName  : "lipeng_3g",
    site : "http://www.hotstation.space"
  }
```
