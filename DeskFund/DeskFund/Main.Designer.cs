﻿namespace DeskFund
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.fundcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.jzrq = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dwjz = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gsz = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gszzl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gztime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(397, 409);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.fundcode,
            this.name,
            this.jzrq,
            this.dwjz,
            this.gsz,
            this.gszzl,
            this.gztime});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.name, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            // 
            // fundcode
            // 
            this.fundcode.Caption = "CODE";
            this.fundcode.FieldName = "Fundcode";
            this.fundcode.Name = "fundcode";
            this.fundcode.Visible = true;
            this.fundcode.VisibleIndex = 0;
            this.fundcode.Width = 61;
            // 
            // name
            // 
            this.name.Caption = "名称";
            this.name.FieldName = "Name";
            this.name.Name = "name";
            this.name.Visible = true;
            this.name.VisibleIndex = 1;
            // 
            // jzrq
            // 
            this.jzrq.Caption = "净值日期";
            this.jzrq.FieldName = "Jzrq";
            this.jzrq.Name = "jzrq";
            // 
            // dwjz
            // 
            this.dwjz.Caption = "单位净值";
            this.dwjz.FieldName = "Dwjz";
            this.dwjz.Name = "dwjz";
            this.dwjz.Visible = true;
            this.dwjz.VisibleIndex = 1;
            this.dwjz.Width = 66;
            // 
            // gsz
            // 
            this.gsz.Caption = "估算净值";
            this.gsz.FieldName = "Gsz";
            this.gsz.Name = "gsz";
            this.gsz.Visible = true;
            this.gsz.VisibleIndex = 2;
            this.gsz.Width = 66;
            // 
            // gszzl
            // 
            this.gszzl.Caption = "估算涨跌";
            this.gszzl.FieldName = "Gszzl";
            this.gszzl.Name = "gszzl";
            this.gszzl.Visible = true;
            this.gszzl.VisibleIndex = 3;
            this.gszzl.Width = 69;
            // 
            // gztime
            // 
            this.gztime.Caption = "估算时间";
            this.gztime.FieldName = "Gztime";
            this.gztime.Name = "gztime";
            this.gztime.Visible = true;
            this.gztime.VisibleIndex = 4;
            this.gztime.Width = 134;
            // 
            // timer2
            // 
            this.timer2.Interval = 10000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 409);
            this.Controls.Add(this.gridControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.LocationChanged += new System.EventHandler(this.MainFormLocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn fundcode;
        private DevExpress.XtraGrid.Columns.GridColumn name;
        private DevExpress.XtraGrid.Columns.GridColumn jzrq;
        private DevExpress.XtraGrid.Columns.GridColumn dwjz;
        private DevExpress.XtraGrid.Columns.GridColumn gsz;
        private DevExpress.XtraGrid.Columns.GridColumn gszzl;
        private DevExpress.XtraGrid.Columns.GridColumn gztime;
        private System.Windows.Forms.Timer timer2;
    }
}